import "./index.css";

import Navbar from "./components/common/header";
import FourColumn from "./components/dependencies/fourcolumn";
import Wrapper from "./components/dependencies/wrapper";
import Footers from "./components/common/footer";
import ThreeCol from "./components/dependencies/threecolumn";

/**
 * Object for FourColumn Section
 * Passing id,class,name object properties
 */
let shortcut_data = [
  { section_id: "html", class: "fa fa-html5", name: "HTML" },
  { section_id: "css", class: "fa fa-css3", name: "CSS" },
  { section_id: "seo", class: "fa fa-search", name: "SEO" },
  { section_id: "social", class: "fa fa-users", name: "Social" },
];

/**
 * Object for Wrapper Section
 */
let shortcuts = [
  {
    artid: "tablet",
    aside_id: "tablet2",
    imglink:
      "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_ipad_vr0pfu.png",
    text:
      " A clean HTML layout and CSS stylesheet making for a great responsive" +
      " framework to design around that includes a responsive drop down" +
      " navigation menu, image slider, contact form and ‘scroll to the top" +
      " jQuery plugin.jQuery plugin.that includes a responsive drop down" +
      "navigation menu, image slider, contact form and ‘scroll to the top’" +
      " jQuery plugin.",
    name: "Mobile.Tablet.Desktop",
    sectionclass: "inner-wrapper",
  },
  {
    artid: "mobile",
    aside_id: "hand-mobile",
    imglink:
      "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_mobile_stt5sh.png",
    text:
      " A clean HTML layout and CSS stylesheet making for a great responsive" +
      " framework to design around that includes a responsive drop down" +
      " navigation menu, image slider, contact form and ‘scroll to the top" +
      " jQuery plugin.jQuery plugin.that includes a responsive drop down" +
      "navigation menu, image slider, contact form and ‘scroll to the top’" +
      " jQuery plugin.",
    name: "Across each device",
    sectionclass: "inner-wrapper-2",
  },
  {
    artid: "",
    aside_id: "desktop",
    imglink:
      "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571256/desktop_fsksnx.png",
    text:
      " A clean HTML layout and CSS stylesheet making for a great responsive" +
      " framework to design around that includes a responsive drop down" +
      " navigation menu, image slider, contact form and ‘scroll to the top" +
      " jQuery plugin.jQuery plugin.that includes a responsive drop down" +
      "navigation menu, image slider, contact form and ‘scroll to the top’" +
      " jQuery plugin.",
    name: "Desktop",
    sectionclass: "inner-wrapper",
  },
];

// Object of One-third Section
let data_object = [
  {
    class: "one-third",
    section_id: "google",
    name: "Google Search",
    Text: "Also included with the Template is the Template Customization Guide with five special video lessons showing you how to get professional website pictures & how to edit them to fit the template,flessons showing you how to get professional website pictures & how to edit them to fit the template,f",
  },
  {
    class: "one-third",
    section_id: "marketing",
    name: "Marketing",
    Text: " Note: this template includes a page with a PHP website contact form and requires a web host or a program such as XAMPP to run PHP and display it`s  content.id = `desktop`,  lessons showing you how to get professional website pictures & how to edit them to fit thetemplate,f",
  },
  {
    class: "one-third",
    section_id: "customers",
    name: "Happy Customers",
    Text: " When you purchase and download The Rocket Design HTML Template you get a full five page responsive HTML website template with both a “light” and “dark” version of the template in addition to the following features:",
  },
];

/**
 * Represents the banner section which has image
 */
function Banner() {
  return (
    <section className="banner">
      <div className="banner-inner">
        <img
          src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571172/rocket_design_k4nzbm.png"
          alt=""
        />
      </div>
    </section>
  );
}

/**
 * Smiley function which has smile icon
 */
function Smiley() {
  return (
    <section id="smiley">
      <h2>: )</h2>
    </section>
  );
}

/**
 * Represents all components
 */
const App = () => {
  // Data of Fourcolumn section

  return (
    <>
      <Navbar />
      <Banner />
      <FourColumn shortcut_data={shortcut_data} />
      <Wrapper shortcuts={shortcuts} />
      <ThreeCol data_object={data_object} />
      <Smiley />
      <Footers />
    </>
  );
};

// will be exposed globally as Component
export default App;
