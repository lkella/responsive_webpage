/**
 * Represents the Inner wrapper section
 * @param {*} props
 * @returns
 */

function Wrapper(props) {
  return (
    <>
      {props.shortcuts.map((data) => {
        if (data.sectionclass === "inner-wrapper") {
          return (
            <FloatingBanner
              sectionclassName={data.sectionclass}
              imglink={data.imglink}
              id={data.id}
              name={data.name}
              text={data.text}
              artid={data.artid}
              key={data.artid}
            />
          );
        } else {
          return (
            <FloatingBannerleft
              sectionclassName={data.sectionclass}
              imglink={data.imglink}
              id={data.id}
              name={data.name}
              text={data.text}
              artid={data.artid}
              key={data.artid}
            />
          );
        }
      })}
    </>
  );
}

/**
 * Represents the section which has Text float right
 * @param {*} props
 * @returns
 */
function FloatingBanner(props) {
  return (
    <>
      <section className={props.sectionclassName}>
        <article id={props.artid}>
          <img src={props.imglink} alt={props.imglink} />
        </article>
        <aside id={props.aside_id}>
          <h2>{props.name}</h2>
          <p>{props.text}</p>
        </aside>
      </section>
    </>
  );
}

/**
 * Represents the section which has Text float left
 * @param {*} props
 * @returns
 */
function FloatingBannerleft(props) {
  return (
    <>
      <section className={props.sectionclassName}>
        <aside id={props.aside_id}>
          <img src={props.imglink} alt={props.imglink} />
        </aside>
        <article id={props.artid}>
          <h2>{props.name}</h2>
          <p>{props.text}</p>
        </article>
      </section>
    </>
  );
}

// will be exposed globally as Component
export default Wrapper;
