/**
 * Represents the One third section
 * @param {*} props
 */

const ThreeCol = (props) => {
  return (
    <>
      <section className="inner-wrapper-3">
        {props.data_object.map((data) => {
          return (
            <section
              className={data.class}
              id={data.section_id}
              key={data.section_id}
            >
              <h3>{data.name}</h3>
              <p>{data.Text}</p>
            </section>
          );
        })}
      </section>
    </>
  );
};

// will be exposed globally as Component
export default ThreeCol;
