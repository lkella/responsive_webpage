/**
 * Represents the one-fourth section
 * Used props to get the data from Main Parent component(App.js)
 */

const FourColumn = (props) => {
  return (
    <>
      {/* Maps the properties of object shortcut_data */}
      {props.shortcut_data.map((data) => {
        return (
          <section
            className="one-fourth"
            id={data.section_id}
            key={data.section_id}
          >
            <i className={data.class}></i>
            <h3>{data.name}</h3>
          </section>
        );
      })}
    </>
  );
};

// will be exposed globally as Component
export default FourColumn;
