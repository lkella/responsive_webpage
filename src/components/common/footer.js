/**
 * Main Footer function
 * @returns Footer1 and Footer2 functions
 */

const Footers = () => {
  return (
    <>
      <Footer1 />
      <Footer2 />
    </>
  );
};

/**
 * Represents the First footer section
 * This section has social links
 */

function Footer1() {
  let footer_data = [
    { class: "fa fa-facebook" },
    { class: "fa fa-google-plus" },
    { class: "fa fa-twitter" },
    { class: "fa fa-youtube" },
    { class: "fa fa-instagram" },
  ];

  return (
    <footer>
      <ul className="social">
        {footer_data.map((data) => {
          return (
            <li key={data.class}>
              <a href="/" target="_blank">
                <i className={data.class}></i>
              </a>
            </li>
          );
        })}
      </ul>
    </footer>
  );
}

/**
 * Represents the second footer section
 * This section has copyrights of websites
 */

function Footer2() {
  return (
    <footer className="second">
      <p>&copy; MaxPower Design</p>
    </footer>
  );
}

// will be exposed globally as Component
export default Footers;
